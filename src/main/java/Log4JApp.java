
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log4JApp {
    private static Logger logger = LogManager.getLogger(Log4JApp.class);

    public static void main(String[] args) {
        logger.trace("Trace message");
        logger.debug("Debug message");
        logger.info("Info message");
        logger.warn("Warn message");
        logger.error("Error message");
        logger.fatal("Fatal message");
    }
}
