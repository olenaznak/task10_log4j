import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SMSSender {
    public static final String ACCOUNT_SID = "AC38f476737b3e53ba907345682aa3e7e2";
    public static final String AUTH_TOKEN = "c5e4d13eeac09712c8fbe337a56b2af5";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380972079331"),
                        new PhoneNumber("+19562846603"), str).create();
    }
}
